<?php
/*
 * Plugin Name: my-Geo-Targeting
 * Author:      Dmitry
 * Version:     Plugin version 1.0
 *
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 *
 */

define('PLUG_DIR', plugin_dir_path( __FILE__ ));

include PLUG_DIR . 'class/ParserCountry.php';
include PLUG_DIR . 'class/GeoTargeting.php';
include PLUG_DIR . 'class/MainGeoTargeting.php';

$parserCountry = new ParserCountry(PLUG_DIR . 'resource/Country.xml');

$geoTargeting = new GeoTargeting($parserCountry);

$MainGeoTargeting = new MainGeoTargeting();

//$MainGeoTargeting->setCountry();

$geoTargeting->setCountry($MainGeoTargeting->country());

$MainGeoTargeting->setCountry($geoTargeting->getCountry());
//echo $geoTargeting->getCountry();